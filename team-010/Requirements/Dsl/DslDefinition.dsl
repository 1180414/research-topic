﻿<?xml version="1.0" encoding="utf-8"?>
<Dsl xmlns:dm0="http://schemas.microsoft.com/VisualStudio/2008/DslTools/Core" dslVersion="1.0.0.0" Id="463b9588-5b63-456a-98a8-3464c74d783c" Description="Description for org.eclipse.Requirements.Requirements" Name="Requirements" DisplayName="Requirements" Namespace="org.eclipse.Requirements" ProductName="Requirements" CompanyName="DEI-ISEP" PackageGuid="3b2d1704-fab3-4144-bb00-c71dbe913177" PackageNamespace="org.eclipse.Requirements" xmlns="http://schemas.microsoft.com/VisualStudio/2005/DslTools/DslDefinitionModel">
  <Classes>
    <DomainClass Id="3068c7d1-f1b9-4078-b9b5-5526567346a2" Description="The root in which all other elements are embedded. Appears as a diagram." Name="Model" DisplayName="Model" Namespace="org.eclipse.Requirements">
      <Properties>
        <DomainProperty Id="cc246bd6-18cb-4170-9783-5deebc7b9626" Description="Description for org.eclipse.Requirements.Model.Title" Name="title" DisplayName="Title">
          <Type>
            <ExternalTypeMoniker Name="/System/String" />
          </Type>
        </DomainProperty>
      </Properties>
      <ElementMergeDirectives>
        <ElementMergeDirective>
          <Notes>Creates an embedding link when an element is dropped onto a model. </Notes>
          <Index>
            <DomainClassMoniker Name="RequirementGroup" />
          </Index>
          <LinkCreationPaths>
            <DomainPath>groups.Elements</DomainPath>
          </LinkCreationPaths>
        </ElementMergeDirective>
      </ElementMergeDirectives>
    </DomainClass>
    <DomainClass Id="a937ab14-1727-4c2a-8000-835e2fa98c0a" Description="Elements embedded in the model. Appear as boxes on the diagram." Name="RequirementGroup" DisplayName="Requirement Group" Namespace="org.eclipse.Requirements">
      <Properties>
        <DomainProperty Id="a020a898-ebf6-47e5-9dce-06a9d1fe57b8" Description="Description for org.eclipse.Requirements.RequirementGroup.Name" Name="name" DisplayName="Name" DefaultValue="" IsElementName="true">
          <Type>
            <ExternalTypeMoniker Name="/System/String" />
          </Type>
        </DomainProperty>
        <DomainProperty Id="98c7358d-7268-4b4d-b3c2-0553a0fe5f71" Description="Description for org.eclipse.Requirements.RequirementGroup.Description" Name="description" DisplayName="Description">
          <Type>
            <ExternalTypeMoniker Name="/System/String" />
          </Type>
        </DomainProperty>
        <DomainProperty Id="81bf7af1-0c8f-42ff-bc33-ce6ee42f60e8" Description="Description for org.eclipse.Requirements.RequirementGroup.Id" Name="id" DisplayName="Id">
          <Type>
            <ExternalTypeMoniker Name="/System/String" />
          </Type>
        </DomainProperty>
      </Properties>
      <ElementMergeDirectives>
        <ElementMergeDirective>
          <Index>
            <DomainClassMoniker Name="Requirement" />
          </Index>
          <LinkCreationPaths>
            <DomainPath>requirements.Requirements</DomainPath>
          </LinkCreationPaths>
        </ElementMergeDirective>
      </ElementMergeDirectives>
    </DomainClass>
    <DomainClass Id="a1e9e64a-c1dd-4332-b328-ecbb0b46b5ed" Description="Description for org.eclipse.Requirements.Requirement" Name="Requirement" DisplayName="Requirement" Namespace="org.eclipse.Requirements">
      <Properties>
        <DomainProperty Id="bb57922a-7f88-4d74-a03d-cb37bc3783b5" Description="Description for org.eclipse.Requirements.Requirement.Title" Name="Title" DisplayName="Title">
          <Type>
            <ExternalTypeMoniker Name="/System/String" />
          </Type>
        </DomainProperty>
        <DomainProperty Id="1de617e5-9fee-42d5-b50d-fa439d993225" Description="Description for org.eclipse.Requirements.Requirement.Description" Name="description" DisplayName="Description">
          <Type>
            <ExternalTypeMoniker Name="/System/String" />
          </Type>
        </DomainProperty>
        <DomainProperty Id="908fede7-6a64-4cdf-96e6-f09d675057a7" Description="Description for org.eclipse.Requirements.Requirement.Type" Name="type" DisplayName="Type">
          <Type>
            <DomainEnumerationMoniker Name="Type" />
          </Type>
        </DomainProperty>
        <DomainProperty Id="83432ae0-1119-4555-8e5a-b6056acf96bc" Description="Description for org.eclipse.Requirements.Requirement.Author" Name="author" DisplayName="Author">
          <Type>
            <ExternalTypeMoniker Name="/System/String" />
          </Type>
        </DomainProperty>
        <DomainProperty Id="3ab9f130-0ee4-4c5d-9890-3bbcb758f4b9" Description="Description for org.eclipse.Requirements.Requirement.Created" Name="created" DisplayName="Created">
          <Type>
            <ExternalTypeMoniker Name="/System/DateTime" />
          </Type>
        </DomainProperty>
        <DomainProperty Id="d4504b54-f59d-4d20-b483-ac80ccc0fff6" Description="Description for org.eclipse.Requirements.Requirement.State" Name="state" DisplayName="State">
          <Type>
            <DomainEnumerationMoniker Name="State" />
          </Type>
        </DomainProperty>
        <DomainProperty Id="5fa9c8d8-a10b-4acf-8065-3cf509f4275f" Description="Description for org.eclipse.Requirements.Requirement.Resolution" Name="resolution" DisplayName="Resolution">
          <Type>
            <DomainEnumerationMoniker Name="Resolution" />
          </Type>
        </DomainProperty>
        <DomainProperty Id="68a86828-d5b7-4dab-b32c-b2ccc261e500" Description="Description for org.eclipse.Requirements.Requirement.Priority" Name="priority" DisplayName="Priority">
          <Type>
            <DomainEnumerationMoniker Name="Priority" />
          </Type>
        </DomainProperty>
      </Properties>
      <ElementMergeDirectives>
        <ElementMergeDirective>
          <Index>
            <DomainClassMoniker Name="Comment" />
          </Index>
          <LinkCreationPaths>
            <DomainPath>comments.Commented</DomainPath>
          </LinkCreationPaths>
        </ElementMergeDirective>
        <ElementMergeDirective>
          <Index>
            <DomainClassMoniker Name="Version" />
          </Index>
          <LinkCreationPaths>
            <DomainPath>_version.Versions</DomainPath>
          </LinkCreationPaths>
        </ElementMergeDirective>
      </ElementMergeDirectives>
    </DomainClass>
    <DomainClass Id="1e570f5f-ac31-4691-aa46-92499aca679d" Description="Description for org.eclipse.Requirements.Comment" Name="Comment" DisplayName="Comment" Namespace="org.eclipse.Requirements">
      <Properties>
        <DomainProperty Id="b97e298e-1310-40d2-b910-93d9bf7c90dd" Description="Description for org.eclipse.Requirements.Comment.Subject" Name="subject" DisplayName="Subject">
          <Type>
            <ExternalTypeMoniker Name="/System/String" />
          </Type>
        </DomainProperty>
        <DomainProperty Id="3f431a01-be11-4443-8eb9-e4adfd87bf4f" Description="Description for org.eclipse.Requirements.Comment.Body" Name="body" DisplayName="Body">
          <Type>
            <ExternalTypeMoniker Name="/System/String" />
          </Type>
        </DomainProperty>
        <DomainProperty Id="bd7a3670-ecc8-4692-ab13-54df19575f5f" Description="Description for org.eclipse.Requirements.Comment.Author" Name="author" DisplayName="Author">
          <Type>
            <ExternalTypeMoniker Name="/System/String" />
          </Type>
        </DomainProperty>
        <DomainProperty Id="078de9a2-8113-43de-bea8-4f7571e9e671" Description="Description for org.eclipse.Requirements.Comment.Created" Name="created" DisplayName="Created">
          <Type>
            <ExternalTypeMoniker Name="/System/DateTime" />
          </Type>
        </DomainProperty>
      </Properties>
    </DomainClass>
    <DomainClass Id="f64b0d36-959a-4931-a922-36ba89b77d10" Description="Description for org.eclipse.Requirements.Version" Name="Version" DisplayName="Version" Namespace="org.eclipse.Requirements">
      <Properties>
        <DomainProperty Id="82df34b4-0754-4198-b2cf-96b44d025974" Description="Description for org.eclipse.Requirements.Version.Major" Name="major" DisplayName="Major">
          <Type>
            <ExternalTypeMoniker Name="/System/Int32" />
          </Type>
        </DomainProperty>
        <DomainProperty Id="5ef3a585-328c-4ef6-9b75-c06756a50cbe" Description="Description for org.eclipse.Requirements.Version.Minor" Name="minor" DisplayName="Minor">
          <Type>
            <ExternalTypeMoniker Name="/System/Int32" />
          </Type>
        </DomainProperty>
        <DomainProperty Id="abc51528-f61a-4fc3-9880-65fb43d86f5e" Description="Description for org.eclipse.Requirements.Version.Service" Name="service" DisplayName="Service">
          <Type>
            <ExternalTypeMoniker Name="/System/Int32" />
          </Type>
        </DomainProperty>
      </Properties>
    </DomainClass>
  </Classes>
  <Relationships>
    <DomainRelationship Id="55fb170e-a74f-4f2a-bf44-0e1ebac7c57b" Description="Embedding relationship between the Model and Elements" Name="groups" DisplayName="Groups" Namespace="org.eclipse.Requirements" IsEmbedding="true">
      <Source>
        <DomainRole Id="5f118979-8a67-4323-89d6-3c5305f0a551" Description="" Name="Model" DisplayName="Model" PropertyName="Elements" PropagatesCopy="PropagatesCopyToLinkAndOppositeRolePlayer" PropertyDisplayName="Elements">
          <RolePlayer>
            <DomainClassMoniker Name="Model" />
          </RolePlayer>
        </DomainRole>
      </Source>
      <Target>
        <DomainRole Id="a96168cf-c759-4f5d-916d-22caca39f98e" Description="" Name="Element" DisplayName="Element" PropertyName="Model" Multiplicity="One" PropagatesDelete="true" PropertyDisplayName="Model">
          <RolePlayer>
            <DomainClassMoniker Name="RequirementGroup" />
          </RolePlayer>
        </DomainRole>
      </Target>
    </DomainRelationship>
    <DomainRelationship Id="ab277b91-50c1-444a-8f97-3497e153323e" Description="Reference relationship between Elements." Name="parent" DisplayName="Parent" Namespace="org.eclipse.Requirements">
      <Source>
        <DomainRole Id="b6eb71a5-870d-4079-aa54-89148b8d65de" Description="Description for org.eclipse.Requirements.ExampleRelationship.Target" Name="Source" DisplayName="Source" PropertyName="Parent" PropertyDisplayName="Parent">
          <RolePlayer>
            <DomainClassMoniker Name="RequirementGroup" />
          </RolePlayer>
        </DomainRole>
      </Source>
      <Target>
        <DomainRole Id="511dcf8a-689c-4fcf-af31-1c318e8f8b27" Description="Description for org.eclipse.Requirements.ExampleRelationship.Source" Name="Target" DisplayName="Target" PropertyName="Children" PropertyDisplayName="Children">
          <RolePlayer>
            <DomainClassMoniker Name="RequirementGroup" />
          </RolePlayer>
        </DomainRole>
      </Target>
    </DomainRelationship>
    <DomainRelationship Id="29f5a3a0-8f16-4b54-8090-a382ede7423b" Description="Description for org.eclipse.Requirements.requirements" Name="requirements" DisplayName="Requirements" Namespace="org.eclipse.Requirements" IsEmbedding="true">
      <Source>
        <DomainRole Id="1554c80d-eb0c-4581-9214-a6ccd5239326" Description="Description for org.eclipse.Requirements.requirements.RequirementGroup" Name="RequirementGroup" DisplayName="Requirement Group" PropertyName="Requirements" PropagatesCopy="PropagatesCopyToLinkAndOppositeRolePlayer" PropertyDisplayName="Requirements">
          <RolePlayer>
            <DomainClassMoniker Name="RequirementGroup" />
          </RolePlayer>
        </DomainRole>
      </Source>
      <Target>
        <DomainRole Id="0a07c59a-cd97-4c41-9d23-0f3cb987fd7a" Description="Description for org.eclipse.Requirements.requirements.Requirement" Name="Requirement" DisplayName="Requirement" PropertyName="RequirementGroup" Multiplicity="One" PropagatesDelete="true" PropertyDisplayName="Requirement Group">
          <RolePlayer>
            <DomainClassMoniker Name="Requirement" />
          </RolePlayer>
        </DomainRole>
      </Target>
    </DomainRelationship>
    <DomainRelationship Id="331a2fe4-278a-44b8-ba64-b3cf0441f8d1" Description="Description for org.eclipse.Requirements.comments" Name="comments" DisplayName="Comments" Namespace="org.eclipse.Requirements" IsEmbedding="true">
      <Source>
        <DomainRole Id="7bb3d921-15f5-4fec-9d02-f628d37b5657" Description="Description for org.eclipse.Requirements.comments.Requirement" Name="Requirement" DisplayName="Requirement" PropertyName="Commented" PropagatesCopy="PropagatesCopyToLinkAndOppositeRolePlayer" PropertyDisplayName="Commented">
          <RolePlayer>
            <DomainClassMoniker Name="Requirement" />
          </RolePlayer>
        </DomainRole>
      </Source>
      <Target>
        <DomainRole Id="0990d890-2ef5-47f8-9025-a7429fe3d3f1" Description="Description for org.eclipse.Requirements.comments.Comment" Name="Comment" DisplayName="Comment" PropertyName="Requirement" Multiplicity="One" PropagatesDelete="true" PropertyDisplayName="Requirement">
          <RolePlayer>
            <DomainClassMoniker Name="Comment" />
          </RolePlayer>
        </DomainRole>
      </Target>
    </DomainRelationship>
    <DomainRelationship Id="8a04f97a-d4ef-40bf-b4f5-27ee5d19eb43" Description="Description for org.eclipse.Requirements.children" Name="children" DisplayName="Children" Namespace="org.eclipse.Requirements">
      <Source>
        <DomainRole Id="a9a86132-8f4d-4d1f-b982-05bff56eabaf" Description="Description for org.eclipse.Requirements.children.SourceComment" Name="SourceComment" DisplayName="Source Comment" PropertyName="TargetCommented" PropertyDisplayName="Target Commented">
          <RolePlayer>
            <DomainClassMoniker Name="Comment" />
          </RolePlayer>
        </DomainRole>
      </Source>
      <Target>
        <DomainRole Id="d57bf9a8-da5a-4925-b100-839f35745e2d" Description="Description for org.eclipse.Requirements.children.TargetComment" Name="TargetComment" DisplayName="Target Comment" PropertyName="SourceCommented" PropertyDisplayName="Source Commented">
          <RolePlayer>
            <DomainClassMoniker Name="Comment" />
          </RolePlayer>
        </DomainRole>
      </Target>
    </DomainRelationship>
    <DomainRelationship Id="765d9310-6671-4cc6-9533-d763203cba97" Description="Description for org.eclipse.Requirements.dependencies" Name="dependencies" DisplayName="Dependencies" Namespace="org.eclipse.Requirements">
      <Source>
        <DomainRole Id="84864030-3d43-4dd4-966a-b503a6d9f251" Description="Description for org.eclipse.Requirements.dependencies.SourceRequirement" Name="SourceRequirement" DisplayName="Source Requirement" PropertyName="TargetRequirements" PropertyDisplayName="Target Requirements">
          <RolePlayer>
            <DomainClassMoniker Name="Requirement" />
          </RolePlayer>
        </DomainRole>
      </Source>
      <Target>
        <DomainRole Id="8154da6c-f230-4c59-a84a-62cbbebcec14" Description="Description for org.eclipse.Requirements.dependencies.TargetRequirement" Name="TargetRequirement" DisplayName="Target Requirement" PropertyName="SourceRequirements" PropertyDisplayName="Source Requirements">
          <RolePlayer>
            <DomainClassMoniker Name="Requirement" />
          </RolePlayer>
        </DomainRole>
      </Target>
    </DomainRelationship>
    <DomainRelationship Id="85737dac-9eb8-44b9-92c9-11775b9575af" Description="Description for org.eclipse.Requirements._version" Name="_version" DisplayName="_version" Namespace="org.eclipse.Requirements" IsEmbedding="true">
      <Source>
        <DomainRole Id="4bf9cc5e-d78e-4208-b72b-a93b119bea9b" Description="Description for org.eclipse.Requirements._version.Requirement" Name="Requirement" DisplayName="Requirement" PropertyName="Versions" PropagatesCopy="PropagatesCopyToLinkAndOppositeRolePlayer" PropertyDisplayName="Versions">
          <RolePlayer>
            <DomainClassMoniker Name="Requirement" />
          </RolePlayer>
        </DomainRole>
      </Source>
      <Target>
        <DomainRole Id="6a199455-0c05-4d1f-9178-f712da84bccd" Description="Description for org.eclipse.Requirements._version.Version" Name="Version" DisplayName="Version" PropertyName="Requirement" Multiplicity="One" PropagatesDelete="true" PropertyDisplayName="Requirement">
          <RolePlayer>
            <DomainClassMoniker Name="Version" />
          </RolePlayer>
        </DomainRole>
      </Target>
    </DomainRelationship>
  </Relationships>
  <Types>
    <ExternalType Name="DateTime" Namespace="System" />
    <ExternalType Name="String" Namespace="System" />
    <ExternalType Name="Int16" Namespace="System" />
    <ExternalType Name="Int32" Namespace="System" />
    <ExternalType Name="Int64" Namespace="System" />
    <ExternalType Name="UInt16" Namespace="System" />
    <ExternalType Name="UInt32" Namespace="System" />
    <ExternalType Name="UInt64" Namespace="System" />
    <ExternalType Name="SByte" Namespace="System" />
    <ExternalType Name="Byte" Namespace="System" />
    <ExternalType Name="Double" Namespace="System" />
    <ExternalType Name="Single" Namespace="System" />
    <ExternalType Name="Guid" Namespace="System" />
    <ExternalType Name="Boolean" Namespace="System" />
    <ExternalType Name="Char" Namespace="System" />
    <DomainEnumeration Name="Type" Namespace="org.eclipse.Requirements" Description="Description for org.eclipse.Requirements.Type">
      <Literals>
        <EnumerationLiteral Description="Description for org.eclipse.Requirements.Type.FUNCTIONAL" Name="FUNCTIONAL" Value="" />
        <EnumerationLiteral Description="Description for org.eclipse.Requirements.Type.NONFUNCTIONAL" Name="NONFUNCTIONAL" Value="" />
      </Literals>
    </DomainEnumeration>
    <DomainEnumeration Name="Priority" Namespace="org.eclipse.Requirements" Description="Description for org.eclipse.Requirements.Priority">
      <Literals>
        <EnumerationLiteral Description="Description for org.eclipse.Requirements.Priority.HIGH" Name="HIGH" Value="" />
        <EnumerationLiteral Description="Description for org.eclipse.Requirements.Priority.MEDIUM" Name="MEDIUM" Value="" />
        <EnumerationLiteral Description="Description for org.eclipse.Requirements.Priority.LOW" Name="LOW" Value="" />
      </Literals>
    </DomainEnumeration>
    <DomainEnumeration Name="Resolution" Namespace="org.eclipse.Requirements" Description="Description for org.eclipse.Requirements.Resolution">
      <Literals>
        <EnumerationLiteral Description="Description for org.eclipse.Requirements.Resolution.INVALID" Name="INVALID" Value="" />
        <EnumerationLiteral Description="Description for org.eclipse.Requirements.Resolution.ACCEPTED" Name="ACCEPTED" Value="" />
        <EnumerationLiteral Description="Description for org.eclipse.Requirements.Resolution.IMPLEMENTED" Name="IMPLEMENTED" Value="" />
        <EnumerationLiteral Description="Description for org.eclipse.Requirements.Resolution.LATER" Name="LATER" Value="" />
      </Literals>
    </DomainEnumeration>
    <DomainEnumeration Name="State" Namespace="org.eclipse.Requirements" Description="Description for org.eclipse.Requirements.State">
      <Literals>
        <EnumerationLiteral Description="Description for org.eclipse.Requirements.State.NEW" Name="NEW" Value="" />
        <EnumerationLiteral Description="Description for org.eclipse.Requirements.State.REVIEWED" Name="REVIEWED" Value="" />
        <EnumerationLiteral Description="Description for org.eclipse.Requirements.State.APPROVED" Name="APPROVED" Value="" />
        <EnumerationLiteral Description="Description for org.eclipse.Requirements.State.RESOLVED" Name="RESOLVED" Value="" />
      </Literals>
    </DomainEnumeration>
  </Types>
  <XmlSerializationBehavior Name="RequirementsSerializationBehavior" Namespace="org.eclipse.Requirements">
    <ClassData>
      <XmlClassData TypeName="Model" MonikerAttributeName="" SerializeId="true" MonikerElementName="modelMoniker" ElementName="model" MonikerTypeName="ModelMoniker">
        <DomainClassMoniker Name="Model" />
        <ElementData>
          <XmlRelationshipData RoleElementName="elements">
            <DomainRelationshipMoniker Name="groups" />
          </XmlRelationshipData>
          <XmlPropertyData XmlName="title">
            <DomainPropertyMoniker Name="Model/title" />
          </XmlPropertyData>
        </ElementData>
      </XmlClassData>
      <XmlClassData TypeName="RequirementGroup" MonikerAttributeName="name" SerializeId="true" MonikerElementName="requirementGroupMoniker" ElementName="requirementGroup" MonikerTypeName="RequirementGroupMoniker">
        <DomainClassMoniker Name="RequirementGroup" />
        <ElementData>
          <XmlPropertyData XmlName="name" IsMonikerKey="true">
            <DomainPropertyMoniker Name="RequirementGroup/name" />
          </XmlPropertyData>
          <XmlRelationshipData RoleElementName="parent">
            <DomainRelationshipMoniker Name="parent" />
          </XmlRelationshipData>
          <XmlPropertyData XmlName="description">
            <DomainPropertyMoniker Name="RequirementGroup/description" />
          </XmlPropertyData>
          <XmlPropertyData XmlName="id">
            <DomainPropertyMoniker Name="RequirementGroup/id" />
          </XmlPropertyData>
          <XmlRelationshipData UseFullForm="true" RoleElementName="requirements">
            <DomainRelationshipMoniker Name="requirements" />
          </XmlRelationshipData>
        </ElementData>
      </XmlClassData>
      <XmlClassData TypeName="Groups" MonikerAttributeName="" SerializeId="true" MonikerElementName="groupsMoniker" ElementName="groups" MonikerTypeName="GroupsMoniker">
        <DomainRelationshipMoniker Name="groups" />
      </XmlClassData>
      <XmlClassData TypeName="Parent" MonikerAttributeName="" SerializeId="true" MonikerElementName="parentMoniker" ElementName="parent" MonikerTypeName="ParentMoniker">
        <DomainRelationshipMoniker Name="parent" />
      </XmlClassData>
      <XmlClassData TypeName="RequirementsDiagram" MonikerAttributeName="" SerializeId="true" MonikerElementName="requirementsDiagramMoniker" ElementName="requirementsDiagram" MonikerTypeName="RequirementsDiagramMoniker">
        <DiagramMoniker Name="RequirementsDiagram" />
      </XmlClassData>
      <XmlClassData TypeName="Requirement" MonikerAttributeName="" SerializeId="true" MonikerElementName="requirementMoniker" ElementName="requirement" MonikerTypeName="RequirementMoniker">
        <DomainClassMoniker Name="Requirement" />
        <ElementData>
          <XmlPropertyData XmlName="title">
            <DomainPropertyMoniker Name="Requirement/Title" />
          </XmlPropertyData>
          <XmlPropertyData XmlName="description">
            <DomainPropertyMoniker Name="Requirement/description" />
          </XmlPropertyData>
          <XmlPropertyData XmlName="type">
            <DomainPropertyMoniker Name="Requirement/type" />
          </XmlPropertyData>
          <XmlPropertyData XmlName="author">
            <DomainPropertyMoniker Name="Requirement/author" />
          </XmlPropertyData>
          <XmlPropertyData XmlName="created">
            <DomainPropertyMoniker Name="Requirement/created" />
          </XmlPropertyData>
          <XmlPropertyData XmlName="state">
            <DomainPropertyMoniker Name="Requirement/state" />
          </XmlPropertyData>
          <XmlPropertyData XmlName="resolution">
            <DomainPropertyMoniker Name="Requirement/resolution" />
          </XmlPropertyData>
          <XmlRelationshipData UseFullForm="true" RoleElementName="commented">
            <DomainRelationshipMoniker Name="comments" />
          </XmlRelationshipData>
          <XmlRelationshipData UseFullForm="true" RoleElementName="targetRequirements">
            <DomainRelationshipMoniker Name="dependencies" />
          </XmlRelationshipData>
          <XmlRelationshipData UseFullForm="true" RoleElementName="versions">
            <DomainRelationshipMoniker Name="_version" />
          </XmlRelationshipData>
          <XmlPropertyData XmlName="priority">
            <DomainPropertyMoniker Name="Requirement/priority" />
          </XmlPropertyData>
        </ElementData>
      </XmlClassData>
      <XmlClassData TypeName="Requirements" MonikerAttributeName="" SerializeId="true" MonikerElementName="requirementsMoniker" ElementName="requirements" MonikerTypeName="RequirementsMoniker">
        <DomainRelationshipMoniker Name="requirements" />
      </XmlClassData>
      <XmlClassData TypeName="Comment" MonikerAttributeName="" SerializeId="true" MonikerElementName="commentMoniker" ElementName="comment" MonikerTypeName="CommentMoniker">
        <DomainClassMoniker Name="Comment" />
        <ElementData>
          <XmlPropertyData XmlName="subject">
            <DomainPropertyMoniker Name="Comment/subject" />
          </XmlPropertyData>
          <XmlPropertyData XmlName="body">
            <DomainPropertyMoniker Name="Comment/body" />
          </XmlPropertyData>
          <XmlPropertyData XmlName="author">
            <DomainPropertyMoniker Name="Comment/author" />
          </XmlPropertyData>
          <XmlPropertyData XmlName="created">
            <DomainPropertyMoniker Name="Comment/created" />
          </XmlPropertyData>
          <XmlRelationshipData UseFullForm="true" RoleElementName="targetCommented">
            <DomainRelationshipMoniker Name="children" />
          </XmlRelationshipData>
        </ElementData>
      </XmlClassData>
      <XmlClassData TypeName="Comments" MonikerAttributeName="" SerializeId="true" MonikerElementName="commentsMoniker" ElementName="comments" MonikerTypeName="CommentsMoniker">
        <DomainRelationshipMoniker Name="comments" />
      </XmlClassData>
      <XmlClassData TypeName="Children" MonikerAttributeName="" SerializeId="true" MonikerElementName="childrenMoniker" ElementName="children" MonikerTypeName="ChildrenMoniker">
        <DomainRelationshipMoniker Name="children" />
      </XmlClassData>
      <XmlClassData TypeName="Dependencies" MonikerAttributeName="" SerializeId="true" MonikerElementName="dependenciesMoniker" ElementName="dependencies" MonikerTypeName="DependenciesMoniker">
        <DomainRelationshipMoniker Name="dependencies" />
      </XmlClassData>
      <XmlClassData TypeName="Version" MonikerAttributeName="" SerializeId="true" MonikerElementName="versionMoniker" ElementName="version" MonikerTypeName="VersionMoniker">
        <DomainClassMoniker Name="Version" />
        <ElementData>
          <XmlPropertyData XmlName="major">
            <DomainPropertyMoniker Name="Version/major" />
          </XmlPropertyData>
          <XmlPropertyData XmlName="minor">
            <DomainPropertyMoniker Name="Version/minor" />
          </XmlPropertyData>
          <XmlPropertyData XmlName="service">
            <DomainPropertyMoniker Name="Version/service" />
          </XmlPropertyData>
        </ElementData>
      </XmlClassData>
      <XmlClassData TypeName="_version" MonikerAttributeName="" SerializeId="true" MonikerElementName="_versionMoniker" ElementName="_version" MonikerTypeName="_versionMoniker">
        <DomainRelationshipMoniker Name="_version" />
      </XmlClassData>
    </ClassData>
  </XmlSerializationBehavior>
  <ExplorerBehavior Name="RequirementsExplorer" />
  <ConnectionBuilders>
    <ConnectionBuilder Name="parentBuilder">
      <Notes>Provides for the creation of an ExampleRelationship by pointing at two ExampleElements.</Notes>
      <LinkConnectDirective>
        <DomainRelationshipMoniker Name="parent" />
        <SourceDirectives>
          <RolePlayerConnectDirective>
            <AcceptingClass>
              <DomainClassMoniker Name="RequirementGroup" />
            </AcceptingClass>
          </RolePlayerConnectDirective>
        </SourceDirectives>
        <TargetDirectives>
          <RolePlayerConnectDirective>
            <AcceptingClass>
              <DomainClassMoniker Name="RequirementGroup" />
            </AcceptingClass>
          </RolePlayerConnectDirective>
        </TargetDirectives>
      </LinkConnectDirective>
    </ConnectionBuilder>
    <ConnectionBuilder Name="childrenBuilder">
      <LinkConnectDirective>
        <DomainRelationshipMoniker Name="children" />
        <SourceDirectives>
          <RolePlayerConnectDirective>
            <AcceptingClass>
              <DomainClassMoniker Name="Comment" />
            </AcceptingClass>
          </RolePlayerConnectDirective>
        </SourceDirectives>
        <TargetDirectives>
          <RolePlayerConnectDirective>
            <AcceptingClass>
              <DomainClassMoniker Name="Comment" />
            </AcceptingClass>
          </RolePlayerConnectDirective>
        </TargetDirectives>
      </LinkConnectDirective>
    </ConnectionBuilder>
    <ConnectionBuilder Name="dependenciesBuilder">
      <LinkConnectDirective>
        <DomainRelationshipMoniker Name="dependencies" />
        <SourceDirectives>
          <RolePlayerConnectDirective>
            <AcceptingClass>
              <DomainClassMoniker Name="Requirement" />
            </AcceptingClass>
          </RolePlayerConnectDirective>
        </SourceDirectives>
        <TargetDirectives>
          <RolePlayerConnectDirective>
            <AcceptingClass>
              <DomainClassMoniker Name="Requirement" />
            </AcceptingClass>
          </RolePlayerConnectDirective>
        </TargetDirectives>
      </LinkConnectDirective>
    </ConnectionBuilder>
  </ConnectionBuilders>
  <Diagram Id="c5d13bfc-6d54-4676-aa49-9bf02adce199" Description="Description for org.eclipse.Requirements.RequirementsDiagram" Name="RequirementsDiagram" DisplayName="Minimal Language Diagram" Namespace="org.eclipse.Requirements">
    <Class>
      <DomainClassMoniker Name="Model" />
    </Class>
  </Diagram>
  <Designer CopyPasteGeneration="CopyPasteOnly" FileExtension="requirementsdsl" EditorGuid="bbf4cba0-23a6-401a-b9cb-b438a4eef37b">
    <RootClass>
      <DomainClassMoniker Name="Model" />
    </RootClass>
    <XmlSerializationDefinition CustomPostLoad="false">
      <XmlSerializationBehaviorMoniker Name="RequirementsSerializationBehavior" />
    </XmlSerializationDefinition>
    <ToolboxTab TabText="Requirements">
      <ElementTool Name="ExampleElement" ToolboxIcon="resources\exampleshapetoolbitmap.bmp" Caption="ExampleElement" Tooltip="Create an ExampleElement" HelpKeyword="CreateExampleClassF1Keyword">
        <DomainClassMoniker Name="RequirementGroup" />
      </ElementTool>
      <ConnectionTool Name="ExampleRelationship" ToolboxIcon="resources\exampleconnectortoolbitmap.bmp" Caption="ExampleRelationship" Tooltip="Drag between ExampleElements to create an ExampleRelationship" HelpKeyword="ConnectExampleRelationF1Keyword">
        <ConnectionBuilderMoniker Name="Requirements/parentBuilder" />
      </ConnectionTool>
    </ToolboxTab>
    <Validation UsesMenu="false" UsesOpen="false" UsesSave="false" UsesLoad="false" />
    <DiagramMoniker Name="RequirementsDiagram" />
  </Designer>
  <Explorer ExplorerGuid="f4141607-9046-444d-ac29-a65922ae0082" Title="Requirements Explorer">
    <ExplorerBehaviorMoniker Name="Requirements/RequirementsExplorer" />
  </Explorer>
</Dsl>