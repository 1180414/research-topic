# EDOM - Project Research Topic

# 1. Introdução

Este trabalho foi elaborado no âmbito da disciplina de EDOM, unidade curricular do mestrado de Engenharia de Software. Este tem como tópico principal "Versioning e Source Control" de projetos de modelação, mais concretamente, projetos que utilizam a framework EMF - Eclipse Modeling Framework, e as grandes diferenças entre este tipo de projetos, e projetos que apenas utilizam artefactos textuais como source code.

## 1.1 Versioning

Consiste na numeração de diferentes versões do software cada vez que este é lançado. Um lançamento pode corresponder a implementação de uma nova funcionalidade ou simplesmente a correção de pequenos bugs. Então, de forma a facilitar a monitorização do que é realmente lançado foram criadas um conjunto de regras que ajuda na separação de lançamentos. Normalmente uma versão é composta por dois ou três números separados por pontos, sendo que o primeiro chamado de major - que é incrementado cada vez que são implementada novas grandes funcionalidades ou melhorias significativas o suficiente para mudar o comportamento do software; o segundo chamado de minor - que é incrementado cada vez que são implementadas funcionalidades de menor importância, ou que suportem a compatibilidade  de versões anteriores, e por fim, o patch que normalmente corresponde a correções de bug menores.
Esta forma de diferenciação de lançamentos não é apenas útil para developers no que toca a controlo do que é implementado, bem como o gerenciamento de dependências e descobrimento de bugs, como também é essencial para o cliente/utilizadores, permitindo que este tenha uma noção da ultima versão que deve utilizar, quando deve atualizar o software, ou apenas comunicar em que versão este está a ter problemas, ou que versão é a mais segura de estar a utilizar.

## 1.2 Source Control

Com o crescimento do código fonte e o número de integrantes no desenvolvimento deste tornou-se essencial que existisse uma forma de monitorar as suas alterações, e manter o código o mais limpo possível. Desta forma foram criados sistemas de controlo de código-fonte, que apresentam um histórico de documentos alterados, versões anteriores de ficheiros, novas alterações a serem implementadas assim como o autor das mudanças, e a data em que estas são feitas de forma a facilitar a sua junção ao código base. Estas práticas vieram ajudar a simplificar o processo de desenvolvimento de software com a introdução de alguns conceitos hoje em dia bastante conhecidos como checkout, merge, commit, entre outros. E está atualmente presente em quase todos os softwares a serem desenvolvidos, sendo que um forte exemplo dele seja o Git.

## 1.3 Source Control em projetos EMF

Sendo que o desenvolvimento de projetos de modelação sofre das mesmas necessidades de um projeto normal de desenvolvimento de software, surgiu a necessidade de existir neles também sistemas de controlo de versões. No entanto, visto que estes projetos consistem fortemente na criação de artefactos de modelos, como diagramas, as soluções existentes como Git não serviam, visto que estes apenas eram capazes de controlar artefactos textuais. Foi então daí que começaram a surgir soluções próprias para projetos EMF.

# 2. Ferramentas de controlo de versões para artefactos de modelos 

Ainda não há uma ferramenta de *versioning* e *source control* para modelos, contudo existem algumas iniciativas promissoras. Todas elas podem ser usadas para modelos de versão UML, mas a maioria das ferramentas são mais genéricas do que isso e suportam vários tipos de modelos (por exemplo, qualquer modelo EMF).

## 2.1 Ferramentas de versioning e source control:

## 2.1.1 EMFStore

O EMFStore é um repositório de modelos para o *Eclipse Modeling Framework* (EMF) e apresenta versioning e source control de modelos.O EMFStore é projetado especificamente para modelos e permite versões semânticas de modelos. Como resultado, ele suporta fusão e detecção de conflitos de forma eficaz. Pode ser usado para instâncias do EMF Model e também pode ser integrado com qualquer tipo de aplicativo existente.

## 2.1.2 CDO  

O CDO é um repositório Java para modelos EMF e meta-modelos. O CDO também pode servir como uma estrutura de persistência e distribuição para sistemas de aplicativos baseados em EMF, inclui suporte de branching e de merging para os modelos.

## 2.1.3 EMFCompare   

O EMFCompare compara modelos da framework EMF. Fornece suporte genérico e personalizável de comparação e merge dos modelos. Implementa uma técnica de comparação de duas fases para modelos de EMF em que a primeira etapa (correspondência) procura as versões do modelo que determinam as relações entre os elementos na versão "nova" e "antiga" e a segunda (diferenciação) procura o resultado correspondente e cria uma lista de diferenças.

# 3. Abordagem através de EMF Store

Sendo esta a ferramenta escolhida para este projeto, será feito um estudo mais profundo da mesma neste capítulo.  
  
O EMFStore é um repositório que serve para guardar, distribuir e colaborar com entidades(dados ou modelos) baseados em EMF. As entidades podem ser partilhadas através de um repositório de servidor/modelo e distribuídas para diferentes clientes. Os clientes modificam as entidades em paralelo e sincronizam com o servidor. Os clientes podem trabalhar offline e os conflitos em alterações simultâneas entre diferentes clientes são detectados e podem ser resolvidos de forma interativa. O servidor mantém um histórico das entidades, que pode ser consultado para recuperar estados e alterações entre diferentes versões das entidades.  
  
## 3.1 Colaboração  
  
Para muitas aplicações, é importante permitir a colaboraçao dos utilizadores nas entidades, permitindo que possam ser feitas alterações nas mesmas. Isto requer sincornização.  
  
O EMFStore fornece os seguintes recursos para colaboração:    

- Sincronização de Clientes com o Servidor
- Detecção de conflitos de alterações simultâneas
- Fusão Interativa
- Autenticação e autorização
- Capacidade Offline  

O EMFStore oferece colaboração offline semelhante a sistemas de versões de códigos-fonte bem conhecidos, como o SVN (Apache Subversion).  
As entidades são armazenadas em projetos. Um projeto pode ser compartilhado com o servidor por um cliente e retirado do servidor para outros clientes. As alterações em projetos compartilhados ou com checkout podem ser sincronizadas com as operações de confirmação(commit/push) e atualização(pull).  
  
As operações de confirmação empurrarão as alterações locais feitas por um cliente para o servidor(remoto) enquanto as operações de atualização puxarão as alterações feitas por outros clientes do servidor(remoto) para o cliente(local). Conflitos podem surgir durante a sincronização se dois clientes alteraram exatamente as mesmas entidades, essas alterações podem ser corrigidas através de uma UI.

Além disso, o EMFStore permite atribuir permissões de utilizadores para fazerem parte dos projetos que contêm as entidades. Os utilizadores são autenticados através de permissões LDAP.

## 3.2 Versioning

O sistema de versão fornece os seguintes recursos:
  
- História refinada das versões
- Diferenciação / Comparação de Versões
- Branching
- Tagging  
  
Toda operação de confirmação(commit/push) para o servidor EMFStore resulta numa nova versão de entidade que é armazenada. O controle de versão permite recuperar qualquer estado da entidade que já tenha sido armazenado no servidor EMFStore em qualquer momento.

Além disso, o histórico da entidade contém um log de alterações refinado em um nível de alteração de atributo. Os clientes EMFStore podem recuperar e exibir diferenças, para comparar diferentes estados das entidades. O log de alterações também armazena informações sobre o utilizador, a hora e a mensagem de log de uma versão.

O histórico da entidade, incluindo branches, é visualizado pelo navegador de histórico do EMFStore.  

O navegador do EMFStore permite visualizar as versões e os branches relacionados com o projeto. Para além disso, o EMFStore suporta marcação para marcar versões especiais e branches para permitir o desenvolvimento simultâneo de entidades. Com os branches, os clientes podem desenvolver diferentes versões da mesma entidade em paralelo e combiná-los posteriormente, fazendo merge. 

## 3.3 User Interface

EMFStore fornece uma interface para quase todas as funcionalidades disponíveis:  
  
- Navegador e Editor (EMF Client Platform Integration)
- Visão Histórica
- Sincronização e interface do utilizador de merge
- Navegador de Repositório (Integração da Plataforma do Cliente EMF)  
  
Com o navegador do repositório (integrado a partir do EMF Client Platform), pode-se configurar os servidores nos quais o cliente pretende conectar e efetuar login nos servidores. Uma visualização no navegador permite visualizar todas as entidades existente na área de trabalho local e o editor reflexivo permite visualizar e modificar elementos da entidade - ambos são apresentados pela *EMF Client Platform*.

Para visualizar a sincronização com o servidor, há diálogos padrão para compartilhar e verificar entidades, bem como caixas de diálogo para confirmar e atualizar entidades. Se um conflito for detectado, as entidades podem ser interativamente *merged* com um diálogo padrão.

Finalmente, pode-se visualizar o histórico de um projeto ou de entidades selecionadas em uma visão histórica. A visualização, também permite ver branches, tags e as alterações entre as versões.

Naturalmente, todos os diálogos podem ser estendidos, personalizados e substituídos conforme necessário.

# 4. Aplicação da abordagem EMF Store

Para exemplificar uma possível aplicação da framework foi utilizado um modelo simples, demonstrado na figura abaixo, que pode ser consultado [aqui](https://github.com/eclipsesource/makeithappen).

![modelo](images/modelo.png)

Depois de aberto no Eclipse o projeto que diz respeito ao modelo, é necessário criar uma *Run Configuration* que permita correr uma aplicação que contenha o modelo associado a si. Para isso é necessário alterar as seguintes configurações:

![modelo](images/configuracao.png)  

![plugins](images/plugins.png)  
  
![pluginsmodelo](images/pluginsmodelo.png)

Depois de executada a configuração, deve ser possível visualizar o *wizard* da aplicação. Através deste wizard deve ser iniciado um servidor EMFStore localmente, seguido do login no mesmo. Este servidor pode ser equiparado a um repositório git e é executado, por defeito, na porta 8080 do url 'localhost'. Depois de realizado o login, é possível configurar os projetos, grupos de utilizadores e utilizadores do servidor.

![iniciarservidor](images/iniciarservidor.png) 

Após a inicialização do servidor, é possível criar um novo projeto dentro da vista 'Model Explorer' *do wizard*. Este projeto pode, desde logo, ser partilhado no servidor através de um clique com o botão direito do rato no mesmo e da seleção da opção 'Share'. Quando selecionada esta opção, é aberta uma *popup* que permite criar um novo repositório ou selecionar um já existente. No nosso caso, deve ser selecionado o repositório criado anteriormente. 
 
Posteriormente, e através novamente do clique com o botão direito do rato no projeto, é possível adicionar elementos do modelo ao projeto. Depois de preenchidos os campos destes elementos, é possível fazer *commit* das alterações para o repositório, devendo ser acrescentada uma mensagem de *commit*. Depois de realizado o commit, é possível visualizar o histórico de alterações do repositório, onde já é apresentado o *commit* acabado de realizar.  
  
![elementosmodelo](images/elementomodelo.png)  
  
![commit](images/commit.png)  
  
![historico](images/historico.png)

É também possível criar um novo *branch*, através da opção 'Create Branch', onde é pedido apenas o nome do novo *branch*. Convém referir que para cada *branch* criado é também criada uma nova versão do repositório. É ainda possivel fazer *merge* com um *branch* já existente, atualizar a versão local do projeto e importar modelos, através de ficheiros com extensão .xmi. 

Na vista de histórico do repositório é também possível adicionar ou remover uma *tag* e realizar o 'checkout' ou 'revert' de um *commit*. 

![adicionartag](images/adicionartag.png)

Finalmente, o EMF Store possui também um *wizard* para a gestão de conflitos, como será explicado mais à frente, que pode ser visualizado na figura seguinte.

![conflitos](images/conflitos.png)

# 5. Git vs Controlo de versões em soluções EMF

O EMFStore oferece colaboração offline semelhante a sistemas de versões de códigos-fonte bem conhecidos, como o SVN. Em contraste com o uso do SVN ou do Git para compartilhar entidades que foram serializadas para arquivos (por exemplo, XML), pode-se usar o EMFStore para compartilhar e fazer checkout de entidades, além de confirmar e atualizar alterações nas entidades. O EMFStore compreende as entidades e a sua estrutura e, portanto, pode suportar detecção de conflitos e merge interativa em um nível de modelo, em oposição a um nível textual. Arquivos XML inválidos após merge ou vincular links nunca podem acontecer ao fazer merge com o EMFStore.

# 6. Revisão de literatura

Nome: Storing and Versioning EMF Models with EMFStore

Autor(es): Maximilian Koegel, Jonas Helming e Nitesh Narayan

Data de publicação: 2010

Descrição: Este artigo é um ficheiro de suporte de um outro tutorial, mas no entanto introduz de forma clara o problema existente relativamente à gestão de modelos e controlo de versões dos mesmos. Inicialmente é exposto o problema que a utilização de sistemas SCM acarreta neste tipo de projetos, seguido de uma breve explicação do que é o EMF, a utilização da framework EMF Store neste contexto e o porquê da escolha desta framework. 

A explicação do funcionamento da EMF Store é bastante concisa e informativa, tocando nos pontos essenciais das funcionalides base desta framework, como por exemplo a forma como é feita a integração do modelo com o repositório num servidor local, e conceitos como *merge*, *commit* e *update*, bem como a resolução de conflitos e o controlo de versões do modelo.

Embora, não tenhamos tido acesso ao tutorial a que o artigo se refere, é possivel que possam ter ficado por explicar alguns passos importantes como a inicialização de um repositório a partir de um servidor local, o *undo* e *revert* de operações - mais conhecidos como *discard* e *revert* do git, o controlo de versões de um repositório da framework e o processo de *checkout* de *branches* do repositório - que é um pouco diferente daquilo que acontece ao utilizar git, sendo criado um novo projeto localmente com as alterações desse branch, que podem ser tópicos importantes para alguém iniciante na utilização da framework que esteja mais familiarizado com o git ou até nenhuma outra ferramenta semelhante.

Em termos gerais, este documento é bastante rico em informação no que toca à necessidade de utilização desta ferramenta bem como o seu funcionamento, no entanto carece da explicação de algumas funcionalidades que estão também presentes no git e que podem ser bastante úteis na utilização desta framework.  