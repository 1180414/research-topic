# EDOM - Project Research Topic

Concerning the course of domain engineer and the technologies studied in the classes, there was a research assignement proposed by the teachers to better improve the knowledge of the students and to create awereness for alternative solutions and technologies to the ones lectured in the course. 

For this matter, our group decided to approach the topic "model validation" and make a study case of the Epsilon framework for model validation as the goal of this research assignement. 

## Model Validation

Models are the very core of the MDSE and while developing our meta-models and defining our models and their properties we would want to make sure they are valid. The more complexity we put into our models and our models, the more we want to make sure that every model is not gona cause problems late on the development lifecycle. This means that we don't want to be developing our models and breaking the rules and constraints set by the meta-model and later find that we have to re-do our development or be completly unaware of what the problems may be. 

Not only is this extremely useful in a debug perspective but also, we can make constraints related to the requirements or the business scenario that we are representing. This helps to keep the models in compliance with our goals. 

This being said, we can state that model validation has a significant impact on the time and quality of the development process and is, therefore, an interesting topic when approaching MDSE and the respective development.

## Model Validation with Epsilon

The goal of EVL is to contribute model validation capabilities to Epsilon. Specifically, EVL is used to detail and evaluate constraints on models of arbitrary metamodels. We will approach EVL implementation, it's abstract and concrete syntax as well as it's execution semantics.

It must be added, although it may have been said in previous paragraphs, the Object Constraint Language (OCL) [3] is the standard language for capturing constraints in modelling languages using object-oriented metamodelling technologies. It's a powerful language and provides means which enables users to specify constraints, it's declarative [3] and gives limitations (as much as it can) of a contemporary model management environment. We will discuss, in detail (as much as we can possible go) the failure of OCL and the promised design of EVL to overcome OCL.

In OCL, constraints are captured in the form of invariants. So, an invariant must be defined in the context of a meta-class of the metamodel and can specify a name and a body (OCL expression with a boolean result that indicates if an instance of the meta-class satisfies the invariant or not) [4]. In terms of execution, the body of an invariant is evaluated for each instance of the meta-class and the results are stored in a set of <Element, Invariant, Boolean> triplets. Each triplet captures the boolean result of an invariant [4]. Next, we present a simply example about OCL constraint on UML operations:

![OCL Constraint with UML Operations](ocl_constraint_with_uml_operations.png)

**1. Observations on OCL**

This section contains a few observations we have made during class on using OCL (please, what we refer describe problems that we encounter but we don't have specific solutions for them).

One of the key points to using OCL is "ease and readability". This may not make much sense, but the OCL specification states that it is a formal language that remains easy to read and write (like the simple code we showed in the previous example). From our experience, this is the closest we get to defining a goal than stating a fact. Whenever a new person got in contact with OCL for the first time, they became very confused.  Short, straight-forward expressions are often very clear, but when the complexity (and size) grows, the clarity and thus readability redically.

So, let's talk about the factors involved. For example, the users aren't as versed in the use of OCL as they are in their preferred programming language. The lack of guidelines for formatting expressions (identation rules), might play a role as well, but it isn't completely a matter of inexperienced users and badly written OCL. Although the clarity can be improved comments and variable definitions, it seems that the syntax is prone to hiding the structure of the expressions. Undoubtedly the obscurity of the OCL specification is partly responsible for the initial confusion in users capacities of facing such complexity language.

We can argue about the fact that the types and operations defined in OCL aren't so extensive as they should be, but we believe, convincingly, there is a much bigger need for smarter ways than *allInstances* to locate data in the model, and that such tools should be included in the standard OCL. For example, the extensions we use to find the operations on our program may not be very clear but, anyways, works for the necessities we want inside OCL.

Again, this is our opinion about OCL and try to have an open mind about it. As a resume:

- No support for **inconsistency detection** for OCL;
- **Frame problem**: operations are specified by what they change (in post-conditions), with the implicit assumption that everything else (the frame) remains unchanged;
- **Limited recursion**;
- **allInstances()** problem: Person.allInstances() allowed and the fact that isn't allowed for **infinite** types such as Integer.allInstances();

Finishing, in OCL there isn't distinctions between errors and warnings and consequently all reported issues are considered to be errors. This is bad because it isn't possible to identify important issues.

**2. No support for dependent constraints**

"Each OCL invariant is a self-contained unit that does not depend on other invariants. There are cases where this design decision is particularly restrictive. For instance consider the invariants I1 and I2 displayed bellow. Both I1 and I2 are applicable on UML classes with I1 requiring that: *the name of a class must not be empty* and I2 requiring that: the name of a class must start with a capital letter. In the case of those two invariants, if I1 is not satisfied for a particular UML class, evaluating I2 on that class would be meaningless. In fact it would be worse than meaningless since it would consume time to evaluate and would also produce an extraneous error message to the user. In practice, to avoid the extraneous message, I2 needs to replicate the body of I1 using an if expression (lines 2 and 5)." [2]

![No Support for Dependent Constraints](no_support_for_dependent_constraints.png)

**3. Abstract Syntax - EVL **

In EVL, the specifications about validations are organized in modules (**EvlModule**). This EvlModule extends the **EolLibraryModule**, but what exactly means these modules? Well, because of the extension library module allowed by EvlModule, it can contain user-defined operations and import the EOL Libraries and EVL modules. Separately, an EVL module contains a "set of invariants grouped by the context they
apply to, and a number of pre and post blocks" [2].

**Context** specifies the types of instances where the contained invariants will be evaluated. Every context can define a guard that limits is applicability to a subset of instances of his own type (this is optional). If the guard fails for an instance of the type, their invariants won't be evaluated.

**Invariant** every EVL invariant defines a name and a body (as previous mentioned) but, optionally, can as well, define a guard (defined in its abstract GuardedElement supertype). Short, straight-forward, "this limits its applicability to a subset of the instances of the type defined by the embracing *context*" [2]. Mention that an invariant can define a number of *fixes*. Invariant is an abstract class used as a super-type for the specific types *Constraint* and *Critique*.

**Guard** used to limit the applicability of invariants. If we talk about *context* approach, it limits the applicability of all the invariants of the same context, but if we go to another road, the *Invariant* approach limits the applicability of a specific invariant.

**Fix** defines a title using *ExpressionOrStatementBlock* instead of a static String to allow users to specify context-aware titles.

Below, we show the ilustration about the **abstract syntax of EVL**:

![Abstract Syntax of EVL](abstract_syntax_of_evl.png)

## Study Case

In our study case, we based in the requirements project implemented during PL classes. Here is a few examples of the constraints we defined.

The first (**HasName**) requires that each **RequirementGroup** has a nono-empty name:

![RequirementGroup - Has Name](requirementgroup_hasname.png)

The second one (**AuthorStartsAWithCapital**), requires that every **requirement attribute** starts with a capital letter. Unlike the HasName, this is declared as a critique which means that if it isn't pleased by an element, it will show a warning on the editor. In the guard of this constraint, we first check that the element satisfies the HasName constraint first (it wouldn't make sense to check this for an empty-named file). If the critique isn't satisfied, a warning is generated and the user is presented with the option to invoke the fix which automatically renames the Requirement attribute:

![Requirement - Author Starts With a Capital Letter](requirement_authorstartswithacapitalletter.png)

The third one (**NameMustStartWithLowerCase**), requires that the author of a **Comment** (we are talking about the attribute in specific) should start with a lowercase letter:

![Comment - Lowercase](comment_lowercase.png)

The final one, verifies if the **body** of the **Comment** has something written, this is, if the value isn't null. Repare that he searchs for him by his **self.id**: 

![Comment - Has a Body](comment_hasabody.png)

## Comparation/ Conclusions

"OCL does not support specifying meaningful messages that can be reported to the user in case an invariant is not satisfied for certain elements." [2]

"Contemporary software development environments typically produce two types of feedback when checking artefacts for consistency and correctness: errors and warnings. Errors indicate critical deficiencies that contradict basic principles and invalidate the developed artefacts. By contrast, warnings (or critiques) indicate non-critical issues that should nevertheless be addressed by the user. To enable users to address warnings in a priority-based manner, they are typically categorized into three levels of importance: High, Medium and Low (although other classifications are also possible). By contrast, in OCL there is no such distinction between errors and warnings and consequently all reported issues are considered to be errors. This adds an additional burden to identifying and prioritizing issues of major importance, particularly within an extensive set of unsatisfied invariants." [2]

OCL has no support for dependente constraints -> "Each OCL invariant is a self-contained unit that does not depend on other invariants." [2]

Limited flexibility in context definition -> "As already discussed, in OCL invariants are defined in the context of meta-classes. While this achieves a reasonable partitioning of the model element space, there are cases where more fine-grained partitioning is required."[2]

"While OCL can be used for detecting inconsistencies, it provides no means for repairing them. The reason is that OCL has been designed as a side-effect free language and therefore lacks constructs for modifying models. Nevertheless, there are many cases where inconsistencies are trivial to resolve and users can benefit from semi-automatic repairing facilities" [2]

## Literature review

- Implementing Efficient Model Validation
in EMF Tools
- "On the Evolution of OCL for Capturing Structural Constraints in Modelling
Languages" 
- https://en.wikipedia.org/wiki/Object_Constraint_Language
- http://www-sop.inria.fr/members/Charles.Andre/CAdoc/ESINSA/UMLOCL-memo.pdf

Made by:

Carlos Vieira nº1140292

Cátia Pinto nº1180408

Tiago Vilaça nº1140412
