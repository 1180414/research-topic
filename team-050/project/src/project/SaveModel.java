package project;

import java.io.IOException;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.EValidator;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.util.Diagnostician;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl;
import org.eclipse.ocl.common.OCLConstants;
import org.eclipse.ocl.pivot.internal.delegate.OCLDelegateDomain;
import org.eclipse.ocl.pivot.internal.delegate.OCLInvocationDelegateFactory;
import org.eclipse.ocl.pivot.internal.delegate.OCLSettingDelegateFactory;
import org.eclipse.ocl.pivot.internal.delegate.OCLValidationDelegateFactory;
import org.eclipse.ocl.pivot.model.OCLstdlib;

import project.ProjectFactory;
import project.Model;

public class SaveModel {

	// Example of how to use the EMF library
	public static void main(String[] args) {
		saveModel();
	}

	public static void initOCL() {
		// -----------------------------------------
		// Initialize Stand alone OCLInEcore
		// The first thing to do before using any code of the model
		String oclDelegateURI = OCLConstants.OCL_DELEGATE_URI;
		EOperation.Internal.InvocationDelegate.Factory.Registry.INSTANCE.put(oclDelegateURI,
				new OCLInvocationDelegateFactory.Global());
		EStructuralFeature.Internal.SettingDelegate.Factory.Registry.INSTANCE.put(oclDelegateURI,
				new OCLSettingDelegateFactory.Global());
		EValidator.ValidationDelegate.Registry.INSTANCE.put(oclDelegateURI, new OCLValidationDelegateFactory.Global());

		OCLDelegateDomain.initialize(null);

		org.eclipse.ocl.xtext.essentialocl.EssentialOCLStandaloneSetup.doSetup();

		OCLstdlib.install();
		// -------------
	}

	public static void saveModel() {
		// Initialize OCL support
		initOCL();

		// Associate the "requirements" extension with the XMI resource format
		Resource.Factory.Registry.INSTANCE.getExtensionToFactoryMap().put("req", new XMIResourceFactoryImpl());

		// Initialize the model
		ProjectPackage.eINSTANCE.eClass();

		// Retrieve the default factory singleton
		ProjectFactory factory = ProjectFactory.eINSTANCE;

		// Create a Model
		Model model = factory.createModel();
		model.setTitle("RequirementsModel");

		// Create Group of Requirements
		RequirementGroup requirementGroup1 = factory.createRequirementGroup();
		requirementGroup1.setId("RG1");
		requirementGroup1.setName("UserReqs");
		RequirementGroup requirementGroup2 = factory.createRequirementGroup();
		requirementGroup2.setId("RG2");
		requirementGroup2.setName("SystemReqs");

		// Create Requirements
		Requirement requirement = factory.createRequirement();
		requirement.setId("R1");
		requirement.setDescription("Requirement1");
		requirement.setTitle("MinimumMeetingTime");
		requirement.setType(Type.NONFUNCTIONAL);
		requirement.setAuthor("Catarina");
		requirement.setPriority(Priority.HIGH);
		requirement.setResolution(Resolution.ACCEPTED);
		requirement.setState(State.NEW);

		Requirement requirement2 = factory.createRequirement();
		requirement2.setId("R2");
		requirement2.setDescription("Requirement2");
		requirement2.setTitle("BackEndLanguage");
		requirement2.setType(Type.FUNCTIONAL);
		requirement2.setAuthor("Catarina");
		requirement2.setPriority(Priority.MEDIUM);
		requirement2.setResolution(Resolution.INVALID);
		requirement2.setState(State.REVIEWED);

		Requirement requirement3 = factory.createRequirement();
		requirement3.setId("R3");
		requirement3.setDescription("Requirement3");
		requirement3.setTitle("DateLibrary");
		requirement3.setType(Type.FUNCTIONAL);
		requirement3.setAuthor("Catarina");
		requirement3.setPriority(Priority.LOW);
		requirement3.setResolution(Resolution.IMPLEMENTEED);
		requirement3.setState(State.RESOLVED);

		// Create Versions
		Version version = factory.createVersion();
		version.setMajor(1);
		version.setMinor(0);
		version.setService(0);
		
		Version version2 = factory.createVersion();
		version.setMajor(1);
		version.setMinor(0);
		version.setService(0);
		
		Version version3 = factory.createVersion();
		version.setMajor(1);
		version.setMinor(0);
		version.setService(0);
		
		System.out.println(version.toString());
		
		// Create comments
		Comment comment = factory.createComment();
		comment.setAuthor("Miguel");
		comment.setBody("This library should replace Java's default Date framework");
		comment.setSubject("DateFramework");
		comment.setCreated(Calendar.getInstance().getTime());
		
		// Add versions to comments
		requirement.setVersion(version);
		requirement2.setVersion(version2);
		requirement3.setVersion(version3);

		requirement3.getComments().add(comment);

		// Add all requirements to the respective Requirements Group
		requirementGroup1.getRequirements().add(requirement);

		requirementGroup2.getRequirements().add(requirement2);
		requirementGroup2.getRequirements().add(requirement3);

		// Add group to model
		model.getGroups().add(requirementGroup1);
		model.getGroups().add(requirementGroup2);

		// Obtain a new resource set
		ResourceSet resSet = new ResourceSetImpl();

		// create a resource
		Resource resource = resSet.createResource(URI.createURI("instances/generatedModel.req"));

		// Model is the main father
		resource.getContents().add(model);

		System.out.println("Group1 requirements:");
		EList<Requirement> functionalRequirements = requirementGroup1.getRequirements();
		for (Requirement r : functionalRequirements) {
			System.out.println(r.getTitle());
		}

		System.out.println("Group2 requirements:");
		EList<Requirement> nonFunctionalRequirements = requirementGroup2.getRequirements();
		for (Requirement r : nonFunctionalRequirements) {
			System.out.println(r.getTitle());
		}

		// Add a call to validation...
		// See:
		// https://stackoverflow.com/questions/8594212/how-to-programmatically-trigger-validation-of-emf-model
		// See: generated code for MindmapValidator.java
		// See:
		// https://mattsch.com/2012/05/31/how-to-use-ocl-when-running-emf-standalone/
		// See: https://wiki.eclipse.org/OCL/OCLinEcore
		System.out.println("Diagnostic:");
		Diagnostic diag = Diagnostician.INSTANCE.validate(model);
		if (diag.getSeverity() != Diagnostic.OK) {
			System.out.println(diag.getMessage());
			List<Diagnostic> l = diag.getChildren();
			for (Diagnostic d : l) {
				System.out.println(d.getMessage());
			}
		} else {
			System.out.println(" Everything seems fine");
		}

		// now save the content.
		try {
			resource.save(Collections.EMPTY_MAP);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
