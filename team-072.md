# Model Transformations
### MODEL TO MODEL TRANSFORMATIONS
![](https://innovationchannel.files.wordpress.com/2014/12/6674957.png?w=300&h=112)


"Model to Model Transformations" são transformações efetuadas a partir de um modelo existente para se chegar a um outro modelo.

As transformações modelo - modelo baseam-se num conjunto de regras de transformação que podem ser especificadas em várias linguagens. Neste trabalho iremos abordar GPL e efetuar uma comparação com o utilizado nas aulas, ATL.


As transformações são efetuadas por um motor de transformação, que, de acordo com as regras definidas através de uma linguagem de transformação, trasmforma um modelo de origem no modelo de destino.

Estas transformações podem-se dividir em:
##### Out-Place transformations:
Neste tipo de transformações o modelo final é um novo modelo criado de raíz aplicando as tranformações ao modelo existente.
##### In-Place transformations:
Neste tipo de transformações o modelo inicial sofre atualizações efetuando-se as alterações necessárias, sendo que o resultado final é o mesmo modelo atualizado e não um novo modelo.

As transformações podem também ser **unidirecionais** ou **bidirecioanis**. No primeiro caso é apenas possivel transformar um modelo origem num destino. No segundo caso, para além de se transformar modelos origem em destino é também possível realizar o inverso. Este método permite, por exemplo, sincronizar modelos.


### General-Purpose Language (GPL)
**GPL**, *General-Purpose Language*  é uma característica de certas linguagens de programação em que estas possibilitam diferentes finalidades de utilização, não estando assim limitadas para um uso específico.
Algumas linguagens de programação são projetadas especialmente para responder uma necessidade específica, estas são chamadas de _Domain-Specific Language_ - **DSL**, pois são feitas para responder às necessidades de um ambiente específico.

__Por exemplo:__

- FORTAN e APL são adequados para programação relacionada para efeitos matemáticos.
- ML, OCAML, Haskell são apropriados para operações de pesquisa.
- Lisp é favorável para o trabalho relacionado à IA.
- Acredita-se que C seja adequado apenas para programas do sistema.

Já as linguagens de programação que podem responder às necessidades de uma ampla variedade de utilização são designadas de GPL.
Essas linguagens podem cumprir mais do que uma finalidade, por exemplo, podem ser aptas para cálculos matemáticos, operações de pesquisa e desenvolvimento de aplicativos ao mesmo tempo.

__Por exemplo:__

- Java pode ser usado para desenvolver páginas da Web interativas, além de criar jogos.
- C ++ pode ser usado para implementar aplicativos, bem como desenvolver programas de sistema.
- Python, Perl, Ruby pode ser usado para programação web, bem como para o desenvolvimento de aplicativos.

Mas, enquanto se aprende sobre GPL, é importante notar que toda linguagem tem sua especialidade. Tal como o Python é melhor como uma linguagem de script, o C ++ é melhor para a programação a nível de sistema.

__Uma lista de várias linguagens de programação GPL:__
- Java
- Rubi
- Python
- C
- C ++
- D
- Dardo
- C #


### Exemplo

A possibilidade de transformação de modelos é uma das grandes vantagens da modelação ao invés do tradicional desenho. Estas transformações podem ser obtidas através de um conjunto de regras usando uma linguagem específica para a transformação (ATL).

Os modelos são definidos por uma linguagem de modelação, definida, por sua vez pela linguagem de um metamodelo.

O seguinte pseudo-código, ilustra uma possível transformação entre modelos

//Transforma Objecto I num Objeto F
Método transformar(Objeto I objInicial) 

	Início
		Declaração do Objecto F objFinal
		
		objFinal.nome = objInicial.nome;
		objFinal.eficiencia = objInicial.resultado / objInicial.gasto
		
		guardar(objFinal)
	Fim

A implementação deste tipo de transformações é realizada idealmente por uma DSL, criada especificamente para transformações entre modelos. Num entanto, é possível usar GPL para o mesmo efeito. Por exemplo, Java e C#.


##JAVA vs ATL
                    
                   
JAVA  | ATL
------------- | -------------
Permite desenvolver programas com diversas finalidades (jogos, páginas Web, etc.) | Amplamente utilizado para fins académicos e industriais
GPL concorrente, baseado em classes, orientado a objetos e projetado especificamente para ter o menor número possível de dependências de implementação. | Suportado por uma ferramenta sazonada
"Write once, run anywhere"- código Java pode ser executado em todas as plataformas que suportam Java | Modelos de origem são de leitura e os modelos de destino são de gravação
Linguagem imperativa  | A linguagem é um híbrido de declarativo com imperativo

### References

Wortmann. (n.d.). An Extensible Component &amp; Connector Architecture Description Infrastructure for Multi-Platform Modeling. Retrieved from www.se-rwth.de/publications/

Küster, J. (n.d.). Model-Driven Software Engineering Foundations of Model-Driven Software Engineering. Retrieved from https://researcher.watson.ibm.com/researcher/files/zurich-jku/mdse-01.pdf

General Purpose Programming Language - Javatpoint. (n.d.). Retrieved from https://www.javatpoint.com/general-purpose-programming-language