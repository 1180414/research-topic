#region Using directives

using System;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Runtime.ConstrainedExecution;

#endregion

//
// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
//
[assembly: AssemblyTitle(@"")]
[assembly: AssemblyDescription(@"")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany(@"edom")]
[assembly: AssemblyProduct(@"requirements")]
[assembly: AssemblyCopyright("")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]
[assembly: System.Resources.NeutralResourcesLanguage("en")]

//
// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Revision and Build Numbers 
// by using the '*' as shown below:

[assembly: AssemblyVersion(@"1.0.0.0")]
[assembly: ComVisible(false)]
[assembly: CLSCompliant(true)]
[assembly: ReliabilityContract(Consistency.MayCorruptProcess, Cer.None)]

//
// Make the Dsl project internally visible to the DslPackage assembly
//
[assembly: InternalsVisibleTo(@"edom.requirements.DslPackage, PublicKey=0024000004800000940000000602000000240000525341310004000001000100398961F1B61CB2BF5EC56DA81C89E91052C1C358E2777421E9739D1D8D25C0089987DBA81B0372586F05BC79D4E9D69AD93C84E82351CBD32250AF565F9923F6AB4BD619202F53000DF1C7A7F4A22C4124C8825697054D4AC33F56360BFDAA593BC486B4B260533B8700DC2DFCE7A2CA92D74955FF02FC71CAD7D747745720F7")]