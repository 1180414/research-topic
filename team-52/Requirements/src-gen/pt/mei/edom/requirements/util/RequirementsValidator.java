/**
 */
package pt.mei.edom.requirements.util;

import java.util.Map;

import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.common.util.DiagnosticChain;
import org.eclipse.emf.common.util.ResourceLocator;

import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.util.EObjectValidator;

import pt.mei.edom.requirements.*;

/**
 * <!-- begin-user-doc -->
 * The <b>Validator</b> for the model.
 * <!-- end-user-doc -->
 * @see pt.mei.edom.requirements.RequirementsPackage
 * @generated
 */
public class RequirementsValidator extends EObjectValidator {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final RequirementsValidator INSTANCE = new RequirementsValidator();

	/**
	 * A constant for the {@link org.eclipse.emf.common.util.Diagnostic#getSource() source} of diagnostic {@link org.eclipse.emf.common.util.Diagnostic#getCode() codes} from this package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.common.util.Diagnostic#getSource()
	 * @see org.eclipse.emf.common.util.Diagnostic#getCode()
	 * @generated
	 */
	public static final String DIAGNOSTIC_SOURCE = "pt.mei.edom.requirements";

	/**
	 * A constant with a fixed name that can be used as the base value for additional hand written constants.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final int GENERATED_DIAGNOSTIC_CODE_COUNT = 0;

	/**
	 * A constant with a fixed name that can be used as the base value for additional hand written constants in a derived class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final int DIAGNOSTIC_CODE_COUNT = GENERATED_DIAGNOSTIC_CODE_COUNT;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RequirementsValidator() {
		super();
	}

	/**
	 * Returns the package of this validator switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EPackage getEPackage() {
		return RequirementsPackage.eINSTANCE;
	}

	/**
	 * Calls <code>validateXXX</code> for the corresponding classifier of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected boolean validate(int classifierID, Object value, DiagnosticChain diagnostics,
			Map<Object, Object> context) {
		switch (classifierID) {
		case RequirementsPackage.MODEL:
			return validateModel((Model) value, diagnostics, context);
		case RequirementsPackage.REQUIREMENT_GROUP:
			return validateRequirementGroup((RequirementGroup) value, diagnostics, context);
		case RequirementsPackage.VERSION:
			return validateVersion((Version) value, diagnostics, context);
		case RequirementsPackage.COMMENT:
			return validateComment((Comment) value, diagnostics, context);
		case RequirementsPackage.REQUIREMENT:
			return validateRequirement((Requirement) value, diagnostics, context);
		case RequirementsPackage.RESOLUTION:
			return validateResolution((Resolution) value, diagnostics, context);
		case RequirementsPackage.PRIORITY:
			return validatePriority((Priority) value, diagnostics, context);
		case RequirementsPackage.TYPE:
			return validateType((Type) value, diagnostics, context);
		case RequirementsPackage.STATE:
			return validateState((State) value, diagnostics, context);
		default:
			return true;
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateModel(Model model, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(model, diagnostics, context))
			return false;
		boolean result = validate_EveryMultiplicityConforms(model, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryDataValueConforms(model, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryReferenceIsContained(model, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryBidirectionalReferenceIsPaired(model, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryProxyResolves(model, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_UniqueID(model, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryKeyUnique(model, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryMapEntryUnique(model, diagnostics, context);
		if (result || diagnostics != null)
			result &= validateModel_mustHaveTitle(model, diagnostics, context);
		if (result || diagnostics != null)
			result &= validateModel_uniqueGroupNames(model, diagnostics, context);
		if (result || diagnostics != null)
			result &= validateModel_uniqueGroupId(model, diagnostics, context);
		return result;
	}

	/**
	 * The cached validation expression for the mustHaveTitle constraint of '<em>Model</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String MODEL__MUST_HAVE_TITLE__EEXPRESSION = "not title.oclIsUndefined()";

	/**
	 * Validates the mustHaveTitle constraint of '<em>Model</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateModel_mustHaveTitle(Model model, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate(RequirementsPackage.Literals.MODEL, model, diagnostics, context,
				"http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot", "mustHaveTitle", MODEL__MUST_HAVE_TITLE__EEXPRESSION,
				Diagnostic.ERROR, DIAGNOSTIC_SOURCE, 0);
	}

	/**
	 * The cached validation expression for the uniqueGroupNames constraint of '<em>Model</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String MODEL__UNIQUE_GROUP_NAMES__EEXPRESSION = "self.groups -> isUnique(name)";

	/**
	 * Validates the uniqueGroupNames constraint of '<em>Model</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateModel_uniqueGroupNames(Model model, DiagnosticChain diagnostics,
			Map<Object, Object> context) {
		return validate(RequirementsPackage.Literals.MODEL, model, diagnostics, context,
				"http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot", "uniqueGroupNames",
				MODEL__UNIQUE_GROUP_NAMES__EEXPRESSION, Diagnostic.ERROR, DIAGNOSTIC_SOURCE, 0);
	}

	/**
	 * The cached validation expression for the uniqueGroupId constraint of '<em>Model</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String MODEL__UNIQUE_GROUP_ID__EEXPRESSION = "self.groups -> isUnique(id)";

	/**
	 * Validates the uniqueGroupId constraint of '<em>Model</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateModel_uniqueGroupId(Model model, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate(RequirementsPackage.Literals.MODEL, model, diagnostics, context,
				"http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot", "uniqueGroupId", MODEL__UNIQUE_GROUP_ID__EEXPRESSION,
				Diagnostic.ERROR, DIAGNOSTIC_SOURCE, 0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateRequirementGroup(RequirementGroup requirementGroup, DiagnosticChain diagnostics,
			Map<Object, Object> context) {
		if (!validate_NoCircularContainment(requirementGroup, diagnostics, context))
			return false;
		boolean result = validate_EveryMultiplicityConforms(requirementGroup, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryDataValueConforms(requirementGroup, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryReferenceIsContained(requirementGroup, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryBidirectionalReferenceIsPaired(requirementGroup, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryProxyResolves(requirementGroup, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_UniqueID(requirementGroup, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryKeyUnique(requirementGroup, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryMapEntryUnique(requirementGroup, diagnostics, context);
		if (result || diagnostics != null)
			result &= validateRequirementGroup_mustHaveName(requirementGroup, diagnostics, context);
		if (result || diagnostics != null)
			result &= validateRequirementGroup_mustHaveDescription(requirementGroup, diagnostics, context);
		if (result || diagnostics != null)
			result &= validateRequirementGroup_uniqueRequirements(requirementGroup, diagnostics, context);
		return result;
	}

	/**
	 * The cached validation expression for the mustHaveName constraint of '<em>Requirement Group</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String REQUIREMENT_GROUP__MUST_HAVE_NAME__EEXPRESSION = "not name.oclIsUndefined()";

	/**
	 * Validates the mustHaveName constraint of '<em>Requirement Group</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateRequirementGroup_mustHaveName(RequirementGroup requirementGroup, DiagnosticChain diagnostics,
			Map<Object, Object> context) {
		return validate(RequirementsPackage.Literals.REQUIREMENT_GROUP, requirementGroup, diagnostics, context,
				"http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot", "mustHaveName",
				REQUIREMENT_GROUP__MUST_HAVE_NAME__EEXPRESSION, Diagnostic.ERROR, DIAGNOSTIC_SOURCE, 0);
	}

	/**
	 * The cached validation expression for the mustHaveDescription constraint of '<em>Requirement Group</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String REQUIREMENT_GROUP__MUST_HAVE_DESCRIPTION__EEXPRESSION = "not description.oclIsUndefined()";

	/**
	 * Validates the mustHaveDescription constraint of '<em>Requirement Group</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateRequirementGroup_mustHaveDescription(RequirementGroup requirementGroup,
			DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate(RequirementsPackage.Literals.REQUIREMENT_GROUP, requirementGroup, diagnostics, context,
				"http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot", "mustHaveDescription",
				REQUIREMENT_GROUP__MUST_HAVE_DESCRIPTION__EEXPRESSION, Diagnostic.ERROR, DIAGNOSTIC_SOURCE, 0);
	}

	/**
	 * The cached validation expression for the uniqueRequirements constraint of '<em>Requirement Group</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String REQUIREMENT_GROUP__UNIQUE_REQUIREMENTS__EEXPRESSION = "self.requirements -> isUnique(id)";

	/**
	 * Validates the uniqueRequirements constraint of '<em>Requirement Group</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateRequirementGroup_uniqueRequirements(RequirementGroup requirementGroup,
			DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate(RequirementsPackage.Literals.REQUIREMENT_GROUP, requirementGroup, diagnostics, context,
				"http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot", "uniqueRequirements",
				REQUIREMENT_GROUP__UNIQUE_REQUIREMENTS__EEXPRESSION, Diagnostic.ERROR, DIAGNOSTIC_SOURCE, 0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateVersion(Version version, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(version, diagnostics, context))
			return false;
		boolean result = validate_EveryMultiplicityConforms(version, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryDataValueConforms(version, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryReferenceIsContained(version, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryBidirectionalReferenceIsPaired(version, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryProxyResolves(version, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_UniqueID(version, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryKeyUnique(version, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryMapEntryUnique(version, diagnostics, context);
		if (result || diagnostics != null)
			result &= validateVersion_allValuesPositive(version, diagnostics, context);
		return result;
	}

	/**
	 * The cached validation expression for the allValuesPositive constraint of '<em>Version</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String VERSION__ALL_VALUES_POSITIVE__EEXPRESSION = "self.major >= 0 and self.minor >=0 and self.service >= 0";

	/**
	 * Validates the allValuesPositive constraint of '<em>Version</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateVersion_allValuesPositive(Version version, DiagnosticChain diagnostics,
			Map<Object, Object> context) {
		return validate(RequirementsPackage.Literals.VERSION, version, diagnostics, context,
				"http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot", "allValuesPositive",
				VERSION__ALL_VALUES_POSITIVE__EEXPRESSION, Diagnostic.ERROR, DIAGNOSTIC_SOURCE, 0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateComment(Comment comment, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment(comment, diagnostics, context))
			return false;
		boolean result = validate_EveryMultiplicityConforms(comment, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryDataValueConforms(comment, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryReferenceIsContained(comment, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryBidirectionalReferenceIsPaired(comment, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryProxyResolves(comment, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_UniqueID(comment, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryKeyUnique(comment, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryMapEntryUnique(comment, diagnostics, context);
		if (result || diagnostics != null)
			result &= validateComment_mustHaveSubject(comment, diagnostics, context);
		if (result || diagnostics != null)
			result &= validateComment_mustHaveBody(comment, diagnostics, context);
		if (result || diagnostics != null)
			result &= validateComment_mustBePostRequirement(comment, diagnostics, context);
		return result;
	}

	/**
	 * The cached validation expression for the mustHaveSubject constraint of '<em>Comment</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String COMMENT__MUST_HAVE_SUBJECT__EEXPRESSION = "not subject.oclIsUndefined()";

	/**
	 * Validates the mustHaveSubject constraint of '<em>Comment</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateComment_mustHaveSubject(Comment comment, DiagnosticChain diagnostics,
			Map<Object, Object> context) {
		return validate(RequirementsPackage.Literals.COMMENT, comment, diagnostics, context,
				"http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot", "mustHaveSubject",
				COMMENT__MUST_HAVE_SUBJECT__EEXPRESSION, Diagnostic.ERROR, DIAGNOSTIC_SOURCE, 0);
	}

	/**
	 * The cached validation expression for the mustHaveBody constraint of '<em>Comment</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String COMMENT__MUST_HAVE_BODY__EEXPRESSION = "not body.oclIsUndefined()";

	/**
	 * Validates the mustHaveBody constraint of '<em>Comment</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateComment_mustHaveBody(Comment comment, DiagnosticChain diagnostics,
			Map<Object, Object> context) {
		return validate(RequirementsPackage.Literals.COMMENT, comment, diagnostics, context,
				"http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot", "mustHaveBody", COMMENT__MUST_HAVE_BODY__EEXPRESSION,
				Diagnostic.ERROR, DIAGNOSTIC_SOURCE, 0);
	}

	/**
	 * The cached validation expression for the mustBePostRequirement constraint of '<em>Comment</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String COMMENT__MUST_BE_POST_REQUIREMENT__EEXPRESSION = "self.created > self.Requirement.created";

	/**
	 * Validates the mustBePostRequirement constraint of '<em>Comment</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateComment_mustBePostRequirement(Comment comment, DiagnosticChain diagnostics,
			Map<Object, Object> context) {
		return validate(RequirementsPackage.Literals.COMMENT, comment, diagnostics, context,
				"http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot", "mustBePostRequirement",
				COMMENT__MUST_BE_POST_REQUIREMENT__EEXPRESSION, Diagnostic.ERROR, DIAGNOSTIC_SOURCE, 0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateRequirement(Requirement requirement, DiagnosticChain diagnostics,
			Map<Object, Object> context) {
		if (!validate_NoCircularContainment(requirement, diagnostics, context))
			return false;
		boolean result = validate_EveryMultiplicityConforms(requirement, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryDataValueConforms(requirement, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryReferenceIsContained(requirement, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryBidirectionalReferenceIsPaired(requirement, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryProxyResolves(requirement, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_UniqueID(requirement, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryKeyUnique(requirement, diagnostics, context);
		if (result || diagnostics != null)
			result &= validate_EveryMapEntryUnique(requirement, diagnostics, context);
		if (result || diagnostics != null)
			result &= validateRequirement_ifResolutionAcceptedToState(requirement, diagnostics, context);
		if (result || diagnostics != null)
			result &= validateRequirement_ifResolutionImplementedToState(requirement, diagnostics, context);
		if (result || diagnostics != null)
			result &= validateRequirement_mustHaveTitle(requirement, diagnostics, context);
		if (result || diagnostics != null)
			result &= validateRequirement_titleSizeMinimum(requirement, diagnostics, context);
		if (result || diagnostics != null)
			result &= validateRequirement_mustHaveDescription(requirement, diagnostics, context);
		if (result || diagnostics != null)
			result &= validateRequirement_descriptionSizeMinimum(requirement, diagnostics, context);
		if (result || diagnostics != null)
			result &= validateRequirement_mustHaveVersion(requirement, diagnostics, context);
		return result;
	}

	/**
	 * The cached validation expression for the ifResolutionAcceptedToState constraint of '<em>Requirement</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String REQUIREMENT__IF_RESOLUTION_ACCEPTED_TO_STATE__EEXPRESSION = "\n"
			+ "\t\t\tif self.resolution = Resolution::ACCEPTED \n" + "\t\t\tthen self.state = State::APPROVED\n"
			+ "\t\t\telse self.state = State::NEW or self.state = State::REVIEWED \n" + "\t\t\tendif";

	/**
	 * Validates the ifResolutionAcceptedToState constraint of '<em>Requirement</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateRequirement_ifResolutionAcceptedToState(Requirement requirement, DiagnosticChain diagnostics,
			Map<Object, Object> context) {
		return validate(RequirementsPackage.Literals.REQUIREMENT, requirement, diagnostics, context,
				"http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot", "ifResolutionAcceptedToState",
				REQUIREMENT__IF_RESOLUTION_ACCEPTED_TO_STATE__EEXPRESSION, Diagnostic.ERROR, DIAGNOSTIC_SOURCE, 0);
	}

	/**
	 * The cached validation expression for the ifResolutionImplementedToState constraint of '<em>Requirement</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String REQUIREMENT__IF_RESOLUTION_IMPLEMENTED_TO_STATE__EEXPRESSION = "\n"
			+ "\t\t\tif self.resolution = Resolution::IMPLEMENTED\n" + "\t\t\tthen self.state = State::RESOLVED\n"
			+ "\t\t\telse self.state = State::NEW or self.state = State::REVIEWED  \n" + "\t\t\tendif";

	/**
	 * Validates the ifResolutionImplementedToState constraint of '<em>Requirement</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateRequirement_ifResolutionImplementedToState(Requirement requirement,
			DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate(RequirementsPackage.Literals.REQUIREMENT, requirement, diagnostics, context,
				"http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot", "ifResolutionImplementedToState",
				REQUIREMENT__IF_RESOLUTION_IMPLEMENTED_TO_STATE__EEXPRESSION, Diagnostic.ERROR, DIAGNOSTIC_SOURCE, 0);
	}

	/**
	 * The cached validation expression for the mustHaveTitle constraint of '<em>Requirement</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String REQUIREMENT__MUST_HAVE_TITLE__EEXPRESSION = "not title.oclIsUndefined()";

	/**
	 * Validates the mustHaveTitle constraint of '<em>Requirement</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateRequirement_mustHaveTitle(Requirement requirement, DiagnosticChain diagnostics,
			Map<Object, Object> context) {
		return validate(RequirementsPackage.Literals.REQUIREMENT, requirement, diagnostics, context,
				"http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot", "mustHaveTitle",
				REQUIREMENT__MUST_HAVE_TITLE__EEXPRESSION, Diagnostic.ERROR, DIAGNOSTIC_SOURCE, 0);
	}

	/**
	 * The cached validation expression for the titleSizeMinimum constraint of '<em>Requirement</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String REQUIREMENT__TITLE_SIZE_MINIMUM__EEXPRESSION = "title.size() > 2";

	/**
	 * Validates the titleSizeMinimum constraint of '<em>Requirement</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateRequirement_titleSizeMinimum(Requirement requirement, DiagnosticChain diagnostics,
			Map<Object, Object> context) {
		return validate(RequirementsPackage.Literals.REQUIREMENT, requirement, diagnostics, context,
				"http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot", "titleSizeMinimum",
				REQUIREMENT__TITLE_SIZE_MINIMUM__EEXPRESSION, Diagnostic.ERROR, DIAGNOSTIC_SOURCE, 0);
	}

	/**
	 * The cached validation expression for the mustHaveDescription constraint of '<em>Requirement</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String REQUIREMENT__MUST_HAVE_DESCRIPTION__EEXPRESSION = "not title.oclIsUndefined()";

	/**
	 * Validates the mustHaveDescription constraint of '<em>Requirement</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateRequirement_mustHaveDescription(Requirement requirement, DiagnosticChain diagnostics,
			Map<Object, Object> context) {
		return validate(RequirementsPackage.Literals.REQUIREMENT, requirement, diagnostics, context,
				"http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot", "mustHaveDescription",
				REQUIREMENT__MUST_HAVE_DESCRIPTION__EEXPRESSION, Diagnostic.ERROR, DIAGNOSTIC_SOURCE, 0);
	}

	/**
	 * The cached validation expression for the descriptionSizeMinimum constraint of '<em>Requirement</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String REQUIREMENT__DESCRIPTION_SIZE_MINIMUM__EEXPRESSION = "description.size() > 10";

	/**
	 * Validates the descriptionSizeMinimum constraint of '<em>Requirement</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateRequirement_descriptionSizeMinimum(Requirement requirement, DiagnosticChain diagnostics,
			Map<Object, Object> context) {
		return validate(RequirementsPackage.Literals.REQUIREMENT, requirement, diagnostics, context,
				"http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot", "descriptionSizeMinimum",
				REQUIREMENT__DESCRIPTION_SIZE_MINIMUM__EEXPRESSION, Diagnostic.ERROR, DIAGNOSTIC_SOURCE, 0);
	}

	/**
	 * The cached validation expression for the mustHaveVersion constraint of '<em>Requirement</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String REQUIREMENT__MUST_HAVE_VERSION__EEXPRESSION = "not self.version.oclIsUndefined()";

	/**
	 * Validates the mustHaveVersion constraint of '<em>Requirement</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateRequirement_mustHaveVersion(Requirement requirement, DiagnosticChain diagnostics,
			Map<Object, Object> context) {
		return validate(RequirementsPackage.Literals.REQUIREMENT, requirement, diagnostics, context,
				"http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot", "mustHaveVersion",
				REQUIREMENT__MUST_HAVE_VERSION__EEXPRESSION, Diagnostic.ERROR, DIAGNOSTIC_SOURCE, 0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateResolution(Resolution resolution, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validatePriority(Priority priority, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateType(Type type, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateState(State state, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * Returns the resource locator that will be used to fetch messages for this validator's diagnostics.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator() {
		// TODO
		// Specialize this to return a resource locator for messages specific to this validator.
		// Ensure that you remove @generated or mark it @generated NOT
		return super.getResourceLocator();
	}

} //RequirementsValidator
